module.exports = {
  apps : [{
    name   : "radiusradio",
    script : "./app.js",
    watch  : '.',
    env    : {
      "NODE_ENV": "development"
    },
    env_production: {
      "NODE_ENV": "production"
    }
  }]
}
