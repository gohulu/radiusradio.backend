'use strict';

module.exports = async (item, createdBy, { userRepository }) => {
    var userExists = await userRepository.getByEmail(item.email);
    if (userExists) {
        throw new Error('User already exists!');
    }
    return userRepository.create(item, createdBy);
};