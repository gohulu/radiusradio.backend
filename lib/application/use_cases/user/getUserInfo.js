'use strict';

module.exports = async (accessToken, { userRepository, accessTokenManager }) => {
  const decoded = accessTokenManager.decode(accessToken);
  if (!decoded) {
    throw new Error('Invalid access token');
  }
  
  return await userRepository.getById(decoded.uid);
};