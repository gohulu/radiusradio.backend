'use strict';

module.exports = async (id, { userRepository }) => {
    if (!id) {
        throw new Error('The user id cannot be null');
    }
    return await userRepository.getById(id);
};