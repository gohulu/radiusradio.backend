'use strict';

module.exports = async (item, updatedBy, { userRepository }) => {
    var current = await userRepository.getById(item.id);

    if (current == null) {
        throw Error('entity does not exists');
    }
    
    return userRepository.update(item, updatedBy);
};