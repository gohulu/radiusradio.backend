'use strict';

module.exports = async (email, { userRepository }) => {
    if (!email) {
        throw new Error('The user email cannot be null');
    }
    return await userRepository.getByEmail(email);
};