'use strict';

const Song = require('../../../domain/models/Song');

module.exports = (songSaveInputViewModel, createdBy, updatedBy, { songRepository }) => {
    var song = new Song(songSaveInputViewModel.id, songSaveInputViewModel.artistId
        , songSaveInputViewModel.genreId, songSaveInputViewModel.name, null
        , createdBy, null, updatedBy);

  return songRepository.save(song);
};