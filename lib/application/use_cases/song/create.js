'use strict';
const fs = require('fs-extra');
const Song = require('../../../domain/models/Song');

module.exports = async (song, createdBy, { songRepository }) => {
    if (song == null) {
        throw new Error('There is a problem creating the song.');
    } 

    return await songRepository.create(song, createdBy);
};