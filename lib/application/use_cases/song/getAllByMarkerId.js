'use strict';

module.exports = (markerId, { songRepository }) => {
  return songRepository.getAllByMarkerId(markerId);
};