'use strict';

module.exports = (id, { songRepository }) => {
  return songRepository.getById(id);
};