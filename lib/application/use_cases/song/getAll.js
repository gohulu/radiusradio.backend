'use strict';

module.exports = ({ songRepository }) => {
  return songRepository.getAll();
};