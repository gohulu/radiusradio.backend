'use strict';
const fs = require('fs-extra');
const Song = require('../../../domain/models/Song');

module.exports = async (id, file, updatedBy, { songRepository }) => {
    if (id == null) {
        throw new Error('Song id is empty.');
    } else if (file == null) {
        throw new Error('There was not found a song to update.');
    }

    var newpath = `./uploadedfiles/songs/${file.filename}`;
    await fs.copy(file.path, newpath);

    return await songRepository.addSongFile(id, newpath, updatedBy);
};