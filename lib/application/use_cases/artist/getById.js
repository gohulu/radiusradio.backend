'use strict';

module.exports = (id, { artistRepository }) => {
  return artistRepository.getById(id);
};