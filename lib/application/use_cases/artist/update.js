'use strict';

module.exports = (item, updatedBy, { artistRepository }) => {
    var current = await artistRepository.getById(item.id);
    if (current == null) {
        throw Error('entity does not exists');
    }
    
    return artistRepository.update(item, updatedBy);
};