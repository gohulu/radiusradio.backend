'use strict';
const constants = require('../../../infrastructure/config/constants');

module.exports = ({ artistRepository }) => {
  return artistRepository.getAll();
};