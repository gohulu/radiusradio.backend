'use strict';

module.exports = (item, createdBy, { artistRepository }) => {
  return artistRepository.create(item, createdBy);
};