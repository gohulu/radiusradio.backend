'use strict';

module.exports = (item, actionBy, { artistRepository }) => {
    return artistRepository.save(item, actionBy);
};