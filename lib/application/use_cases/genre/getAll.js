'use strict';

module.exports = ({ genreRepository }) => {
  return genreRepository.getAll();
};