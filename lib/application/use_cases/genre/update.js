'use strict';

module.exports = (item, updatedBy, { genreRepository }) => {
  return genreRepository.update(item, updatedBy);
};