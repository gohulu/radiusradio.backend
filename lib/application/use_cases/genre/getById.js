'use strict';

module.exports = (id, { genreRepository }) => {
  return genreRepository.getById(id);
};