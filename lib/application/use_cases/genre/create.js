'use strict';

module.exports = (item, createdBy, { genreRepository }) => {
  return genreRepository.create(item, createdBy);
};