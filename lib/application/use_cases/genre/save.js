'use strict';

module.exports = (item, actionBy, { genreRepository }) => {
  return genreRepository.save(item, actionBy);
};

