'use strict';

module.exports = async (name, { roleRepository }) => {
    if (name == null) {
        throw new Error('The role name cannot be empty.');
    }

    var roleExists = await roleRepository.getByName(name);
    if (!roleExists) {
        return await roleRepository.create(name);
    }
    return roleExists;
};