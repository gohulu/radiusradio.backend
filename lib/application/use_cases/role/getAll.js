'use strict';

module.exports = ({ roleRepository }) => {
  return roleRepository.getAll();
};