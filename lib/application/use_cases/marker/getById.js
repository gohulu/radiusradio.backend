'use strict';

module.exports = (id, { markerRepository }) => {
  return markerRepository.getById(id);
};