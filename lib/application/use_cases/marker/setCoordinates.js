'use strict';

module.exports = (coordinatesUpdateViewModel, { markerRepository }) => {
  return markerRepository.setCoordinates(coordinatesUpdateViewModel);
};