'use strict';

module.exports = ({ markerRepository }) => {
  return markerRepository.getAll();
};