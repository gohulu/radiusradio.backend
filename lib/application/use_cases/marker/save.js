'use strict';

const Marker = require('../../../domain/models/Marker');

module.exports = (markerSaveInputViewModel, createdBy, updatedBy, { markerRepository }) => {
    var marker = new Marker(markerSaveInputViewModel.id, markerSaveInputViewModel.latitude
        , markerSaveInputViewModel.longitude, null, createdBy, null, updatedBy
        , markerSaveInputViewModel.description, markerSaveInputViewModel.songs);
  return markerRepository.save(marker);
};