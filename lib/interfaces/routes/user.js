'use strict';

const Joi = require('joi');
const Usercontroller = require('../controllers/UserController');
const UserAddInputViewModel = require('../../infrastructure/viewmodels/input/UserAddInputViewModel');
const UserUpdateInputViewModel = require('../../infrastructure/viewmodels/input/UserUpdateInputViewModel');

module.exports = {
  name: 'users',
  version: '1.0.0',
  register: async (server) => {
    server.route([
      {
        method: 'GET',
        path: '/user',
        handler: Usercontroller.getAll,
        options: {
          auth: 'oauth-jwt',
          description: 'Get All Users',
          tags: ['api'],
        },
      },
      {
        method: 'POST',
        path: '/user',
        handler: Usercontroller.addUser,
        options: {
          auth: 'oauth-jwt',
          description: 'Create user',
          tags: ['api'],
          validate: {
            payload: UserAddInputViewModel
          }
        },
      },
      {
        method: 'PUT',
        path: '/user',
        handler: Usercontroller.updateUser,
        options: {
          auth: 'oauth-jwt',
          description: 'Update user',
          tags: ['api'],
          validate: {
            payload: UserUpdateInputViewModel
          }
        },
      },
      {
        method: 'GET',
        path: '/user/{id}',
        handler: Usercontroller.get,
        options: {
          auth: 'oauth-jwt',
          description: 'Get User',
          tags: ['api'],
          validate: {
            params: Joi.object({
              id : Joi.number()
                      .required()
                      .description('UserId to retrieve the User'),
            })
          }
        },
      },
    ]);
  }
};