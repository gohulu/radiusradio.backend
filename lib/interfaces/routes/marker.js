'use strict';

const Joi = require('joi');
const MarkerController = require('../controllers/MarkerController');
const MarkerSaveInputViewModel = require('../../infrastructure/viewmodels/input/MarkerSaveInputViewModel');
const MarkerDetailViewModelJoi = require('../../infrastructure/viewmodels/output/joi/MarkerDetailViewModelJoi');
const MarkerDetailArrayViewModelJoi = require('../../infrastructure/viewmodels/output/joi/MarkerDetailArrayViewModelJoi');

module.exports = {
  name: 'marker',
  version: '1.0.0',
  register: async (server) => {
    server.route([
        {
          method: 'GET',
          path: '/marker/{id}',
          handler: MarkerController.getById,
          options: {
            description: 'Get Marker by its {Id}',
            tags: ['api'],
            auth: 'oauth-jwt',
            validate: {
              params: Joi.object({
                  id : Joi.number()
                          .required()
                          .description('the id for the todo item'),
              })
            },
            response: { 
              status: {
                200: MarkerDetailViewModelJoi
              }
            }
          },
        },
        {
          method: 'GET',
          path: '/marker',
          handler: MarkerController.getAll,
          options: {
            auth: 'oauth-jwt',
            description: 'Get All Markers',
            tags: ['api'],
            response: { 
              status: {
                200: MarkerDetailArrayViewModelJoi
              }
            }
          }
        },
        {
          method: 'POST',
          path: '/marker',
          handler: MarkerController.setCoordinates,
          options: {
            auth: 'oauth-jwt',
            description: 'Save Genre',
            tags: ['api'],
            validate: {
              payload: MarkerSaveInputViewModel
            },
            response: { 
              status: {
                200: MarkerDetailViewModelJoi
              }
            }
          }
        },
        {
          method: 'POST',
          path: '/marker/setCoordinates',
          handler: MarkerController.save,
          options: {
            auth: 'oauth-jwt',
            description: 'Save Genre',
            tags: ['api'],
            validate: {
              payload: MarkerSaveInputViewModel
            },
            response: { 
              status: {
                200: MarkerDetailViewModelJoi
              }
            }
          }
        }
      ]);
  }
};