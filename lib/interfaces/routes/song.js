'use strict';

const Joi = require('joi');
const SongController = require('../../interfaces/controllers/SongController');
const SongSaveInputViewModel = require('../../infrastructure/viewmodels/input/SongSaveInputViewModel');
const SongArrayOutputViewModel = require('../../infrastructure/viewmodels/output/joi/SongArrayOutputViewModelJoi');
const SongAddInputViewModel = require('../../infrastructure/viewmodels/input/SongAddInputViewModel');
const SongOutputViewModelJoi = require('../../infrastructure/viewmodels/output/joi/SongOutputViewModelJoi');

module.exports = {
  name: 'songs',
  version: '1.0.0',
  register: async (server) => {

  server.route([
  {
    method: 'GET',
    path: '/song/{id}',
    handler: SongController.getById,
    options: {
      description: 'Get Song by its {Id}',
      tags: ['api'],
      auth: 'oauth-jwt',
      validate: {
        params: Joi.object({
          id : Joi.number()
                  .required()
                  .description('The id of the song'),
        })
      },
      response: { 
        status: {
          200: SongOutputViewModelJoi
        }
      }
    },
  },
  {
    method: 'GET',
    path: '/song',
    handler: SongController.getAll,
    options: {
      auth: 'oauth-jwt',
      description: 'Get All Songs',
      tags: ['api'],
      response: { 
        status: {
          200: SongArrayOutputViewModel 
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/song/save',
    handler: SongController.save,
    options: {
      auth: 'oauth-jwt',
      description: 'Save Song',
      tags: ['api'],
      validate: {
        payload: SongSaveInputViewModel
      },
      response: { 
        status: {
          200: SongOutputViewModelJoi
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/song/getByMarkerId/{id}',
    handler: SongController.getAllByMarkerId,
    options: {
      auth: 'oauth-jwt',
      description: 'Get Song by the MarkerId',
      tags: ['api'],
      validate: {
        params: Joi.object({
          id : Joi.number()
                  .required()
                  .description('MarkerId to retrieve the song'),
        })
      },
      response: { 
        status: {
          200: SongArrayOutputViewModel
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/song/uploadfile',
    handler: async (request, reply) => {
      return await SongController.uploadSong(request);
    },
    options: {
      auth: 'oauth-jwt',
      description: 'Upload Song File',
      tags: ['api'],
      payload: {
        maxBytes: 209715200,
        output: 'file',
        parse: true,
        allow: 'multipart/form-data',
        multipart: true
      },
      response: { 
        status: {
          200: SongOutputViewModelJoi
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/song',
    handler: async (request, reply) => {
      return await SongController.create(request);
    },
    options: {
      auth: 'oauth-jwt',
      description: 'Create Song',
      tags: ['api'],
      validate: {
        payload: SongAddInputViewModel
      },
      response: { 
        status: {
          200: SongOutputViewModelJoi
        }
      }
    }
  },
  ]);
  }
};