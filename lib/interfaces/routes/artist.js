'use strict';

const Joi = require('joi');
const ArtistSaveInputModel = require('../../infrastructure/viewmodels/input/ArtistSaveInputModel');
const ArtistController = require('../controllers/ArtistController');
const ArtistDetailViewModel = require('../../infrastructure/viewmodels/output/joi/ArtistDetailViewModelJoi');
const ArtistDetailArrayViewModelJoi = require('../../infrastructure/viewmodels/output/joi/ArtistDetailArrayViewModelJoi');

module.exports = {
  name: 'artists',
  version: '1.0.0',
  register: async (server) => {
    server.route([
      {
        method: 'GET',
        path: '/artist/{id}',
        handler: ArtistController.getById,
        options: {
          auth: 'oauth-jwt',
          description: 'Get Artist by its {Id}',
          tags: ['api'],
          validate: {
            params: Joi.object({
                id : Joi.number()
                        .required()
                        .description('the id for the todo item'),
            })
          },
          response: { 
            status: {
              200: ArtistDetailViewModel
            }
          }
        },
      },
      {
        method: 'GET',
        path: '/artist',
        handler: ArtistController.getAll,
        options: {
          auth: 'oauth-jwt',
          description: 'Get All Artists',
          tags: ['api'],
          response: { 
            status: {
              200: ArtistDetailArrayViewModelJoi
            }
          }
        },
      },
      {
        method: 'POST',
        path: '/artist',
        handler: ArtistController.save,
        options: {
          auth: 'oauth-jwt',
          description: 'Save Artist',
          tags: ['api'],
          validate: {
            payload: ArtistSaveInputModel
          },
          response: { 
            status: {
              200: ArtistDetailViewModel
            }
          }
        }
      }
    ]);
  }
};