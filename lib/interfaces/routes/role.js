'use strict';

const RoleController = require('../controllers/RoleController');

module.exports = {
  name: 'roles',
  version: '1.0.0',
  register: async (server) => {
    server.route([
      {
        method: 'GET',
        path: '/role',
        handler: RoleController.getAll,
        options: {
          auth: 'oauth-jwt',
          description: 'Get All Roles',
          tags: ['api'],
        },
      },
    ]);
  }
};