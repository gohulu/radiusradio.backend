'use strict';

const Joi = require('joi');
const GenreController = require('../controllers/GenreController');
const GenreSaveInputModel = require('../../infrastructure/viewmodels/input/GenreSaveInputModel');
const GenreDetailViewModelJoi = require('../../infrastructure/viewmodels/output/joi/GenreDetailViewModelJoi');
const GenreDetailArrayViewModelJoi = require('../../infrastructure/viewmodels/output/joi/GenreDetailArrayViewModelJoi');

module.exports = {
  name: 'genre',
  version: '1.0.0',
  register: async (server) => {
    server.route([
        {
          method: 'GET',
          path: '/genre/{id}',
          handler: GenreController.getById,
          options: {
            auth: 'oauth-jwt',
            description: 'Get Genre by its {Id}',
            tags: ['api'],
            validate: {
              params: Joi.object({
                  id : Joi.number()
                          .required()
                          .description('the id for the todo item'),
              })
            },
            response: { 
              status: {
                200: GenreDetailViewModelJoi
              }
            }
          },
        },
        {
          method: 'GET',
          path: '/genre',
          handler: GenreController.getAll,
          options: {
            auth: 'oauth-jwt',
            description: 'Get All Genres',
            tags: ['api'],
            response: { 
              status: {
                200: GenreDetailArrayViewModelJoi
              }
            }
          }
        },
        {
          method: 'POST',
          path: '/genre',
          handler: GenreController.save,
          options: {
            auth: 'oauth-jwt',
            description: 'Save Genre',
            tags: ['api'],
            validate: {
              payload: GenreSaveInputModel
            },
            response: { 
              status: {
                200: GenreDetailViewModelJoi
              }
            }
          }
        }
      ]);
  }
};