const getById = require('../../application/use_cases/song/getById');
const getAll = require('../../application/use_cases/song/getAll');
const save = require('../../application/use_cases/song/save');
const getAllByMarkerId = require('../../application/use_cases/song/getAllByMarkerId');
const addSongFile = require('../../application/use_cases/song/addSongFile');
const create = require('../../application/use_cases/song/create');

module.exports = {
    async getById(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const id = request.params.id;
        const song = await getById(id, serviceLocator);
        return serviceLocator.songSerializer.serialize(song);
    },
    async getAll(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await getAll(serviceLocator);
    },
    async create(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await create(request.payload, request.auth.credentials.uid, serviceLocator);
    },
    async save(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const song = await save(serviceLocator);
        return serviceLocator.genreSerializer.serialize(song);
    },
    async getAllByMarkerId(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const markerId = request.params.id;
        return await getAllByMarkerId(markerId, serviceLocator);
    },
    async uploadSong(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const file = request.payload.file;
        const id = request.payload.id;
        return await addSongFile(id, file, request.auth.credentials.uid, serviceLocator);
    }
}