const getAll = require('../../application/use_cases/role/getAll');

module.exports = {
    async getAll(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await getAll(serviceLocator);
    },
}