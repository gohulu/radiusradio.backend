const getById = require('../../application/use_cases/marker/getById');
const getAll = require('../../application/use_cases/marker/getAll');
const save = require('../../application/use_cases/marker/save');
const setCoordinates = require('../../application/use_cases/marker/setCoordinates');

module.exports = {
    async getById(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const id = request.params.id;
        return await getById(id, serviceLocator);
    },
    async getAll(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await getAll(serviceLocator);
    },
    async save(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await save(request.payload, "admin", "admin", serviceLocator);
    },
    async setCoordinates(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await setCoordinates(request.payload, serviceLocator);
    }
}