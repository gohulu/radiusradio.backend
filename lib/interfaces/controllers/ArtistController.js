const getById = require("../../application/use_cases/artist/getById");
const getAll = require('../../application/use_cases/artist/getAll');
const save = require('../../application/use_cases/artist/save');

module.exports = {
    async getById(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const id = request.params.id;
        return await getById(id, serviceLocator);
    },
    async getAll(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await getAll(serviceLocator);
    },
    async save(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await save(request.payload, "admin", serviceLocator);
    }
}