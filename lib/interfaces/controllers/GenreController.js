const getById = require('../../application/use_cases/genre/getById');
const getAll = require('../../application/use_cases/genre/getAll');
const create = require("../../application/use_cases/genre/create");
const save = require("../../application/use_cases/genre/save");

module.exports = {
    async getById(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const id = request.params.id;
        return await getById(id, serviceLocator);
    },
    async getAll(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await getAll(serviceLocator);
    },
    async create(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const genres = await create(serviceLocator);
        return serviceLocator.genreSerializer.serialize(genres);
    },
    async save(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await save(request.payload, 'admin', serviceLocator);
    }
}