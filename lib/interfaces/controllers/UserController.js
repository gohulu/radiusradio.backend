const getAll = require('../../application/use_cases/user/getAll');
const addUser = require('../../application/use_cases/user/addUser');
const updateUser = require('../../application/use_cases/user/updateUser');
const getUser = require('../../application/use_cases/user/getUser');

module.exports = {
    async getAll(request) {
        const serviceLocator = request.server.app.serviceLocator;
        return await getAll(serviceLocator);
    },
    async addUser(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const item = request.payload;
        return await addUser(item, 'admin', serviceLocator);
    },
    async updateUser(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const item = request.payload;
        return await updateUser(item, 'admin', serviceLocator);
    },
    async get(request) {
        const serviceLocator = request.server.app.serviceLocator;
        const id = request.params.id;
        return await getUser(id, serviceLocator);
    }
}