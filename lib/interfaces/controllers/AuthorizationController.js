'use strict';

const Boom = require('@hapi/boom');
const GetAccessToken = require('../../application/use_cases/authorization/GetAccesToken');
const VerifyAccessToken = require('../../application/use_cases/authorization/VerifyAccessToken');
const getUserInfo = require('../../application/use_cases/user/getUserInfo');

module.exports = {

  async getAccessToken(request) {
    const serviceLocator = request.server.app.serviceLocator;
    // const grantType = request.payload['grant_type'];
    const email = request.payload['username'];
    const password = request.payload['password'];

    // if (!grantType || grantType !== 'password') {
    //   return Boom.badRequest('Invalid authentication strategy');
    // }

    try {
      var token = await GetAccessToken(email, password, serviceLocator);
      return token;
    } catch (err) {
      return Boom.unauthorized('Bad credentials');
    }
  },

  verifyAccessToken(request, h) {
    // Context
    const serviceLocator = request.server.app.serviceLocator;

    // Input
    const authorizationHeader = request.headers.authorization;
    if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
      throw Boom.badRequest('Missing or wrong Authorization request header', 'oauth');
    }
    const accessToken = authorizationHeader.replace(/Bearer/gi, '').replace(/ /g, '');

    // Treatment
    try {
      const { uid } = VerifyAccessToken(accessToken, serviceLocator);

      // Output
      return h.authenticated({
        credentials: { uid },
        artifacts: { accessToken: accessToken }
      });
    } catch (err) {
      return Boom.unauthorized('Bad credentials');
    }
  },

  async getUserInfo(request) {
    const serviceLocator = request.server.app.serviceLocator;
    const authorizationHeader = request.headers.authorization;
    if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
      throw Boom.badRequest('Missing or wrong Authorization request header', 'oauth');
    }
    const accessToken = authorizationHeader.replace(/Bearer/gi, '').replace(/ /g, '');

    try {
      return await getUserInfo(accessToken, serviceLocator);
    } catch (err) {
      return Boom.unauthorized('Bad credentials');
    }
  }
};