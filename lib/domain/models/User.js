'use strict';

module.exports = class {
    constructor(id = null, firstName, lastName, email, password, active, isLocked, roleId, lastAccessDate, createdAt, createdBy, updatedAt, updatedBy) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.active = active;
        this.isLocked = isLocked;
        this.roleId = roleId;
        this.lastAccessDate = lastAccessDate;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }
};