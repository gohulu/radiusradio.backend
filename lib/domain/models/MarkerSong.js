'use strict';

module.exports = class {
    constructor(markerId, songId, createdAt) {
        this.markerId = markerId;
        this.songId = songId;
        this.createdAt = createdAt;
    }
};