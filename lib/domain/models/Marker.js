'use strict';

module.exports = class {
    constructor(id = null, latitude, longitude, createdAt, createdBy, updatedAt, updatedBy, description, songs) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
        this.description = description;
        this.songs = songs
    }
};