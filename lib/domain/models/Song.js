'use strict';

module.exports = class {
    constructor(id = null, artistId, genreId, name, createdAt, createdBy, updatedAt, updatedBy, description) {
        this.id = id;
        this.artistId = artistId;
        this.genreId = genreId;
        this.name = name;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
        this.description = description;
    }
};