'use strict';

module.exports = class {
    constructor(id = null, name, createdAt, createdBy, updatedAt, updatedBy) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }
};