'use strict';

module.exports = class {
    getById(id) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }

    getAll(page, pageSize, search) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }

    getAllByMarkerId(markerId) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }

    create(item, createdBy) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }

    update(item, updatedBy) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }

    save(item, actionBy) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }

    addSongFile(id, filePath, actionBy) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }
};
