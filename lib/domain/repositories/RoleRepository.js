'use strict';

module.exports = class {
    getAll() {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    };

    getByName(name) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    };

    create(name) {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
    }
};
