'use strict';

const Hapi = require('@hapi/hapi');
const Good = require('@hapi/good');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const Blipp = require('blipp');
const HapiSwagger = require('hapi-swagger');
const Package = require('../../../package');
const addUser = require('../../application/use_cases/user/addUser');
const addRole = require('../../application/use_cases/role/addRole');
const getByEmail = require('../../application/use_cases/user/getByEmail');

const createServer = async () => {
  const server = Hapi.server({
    port: process.env.PORT || 3000,
    routes: {
      cors: {
        origin: [
          `http://localhost:${process.env.PORT || 3000}`,
          `http://localhost`
        ],
        additionalHeaders: [
          'Access-Control-Allow-Origin',
          'Access-Control-Request-Method',
          'Allow-Origin',
          'Origin',
          'access-control-allow-origin',
          'access-control-request-method',
          'allow-origin',
          'origin',
        ]
      },
      validate: {
        failAction: (request, h, err) => {
          throw err;
        }
      }
    }
  });

  await server.register([
    Blipp,
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: {
        info: {
          title: 'Radius Radio API',
          version: Package.version,
        },
        securityDefinitions: {
          'jwt': {
            "type": "oauth2",
            "tokenUrl": "/oauth/token",
            "flow": "password",
            "scopes": {
              "api": "Radius Radio API"
            }
          }
        },
        security: [{ jwt: [] }],
      },
      jsonPath: '/swagger.json'
    },
    {
      plugin: Good,
      options: {
        ops: {
          interval: 1000 * 60
        },
        reporters: {
          myConsoleReporter: [
            {
              module: '@hapi/good-squeeze',
              name: 'Squeeze',
              args: [{ ops: '*', log: '*', error: '*', response: '*' }]
            },
            {
              module: '@hapi/good-console'
            },
            'stdout'
          ]
        }
      },
    },
  ]);

  await server.register([ require('./oauth') ]);
  await server.register({ plugin: require('../../interfaces/routes/artist'), routes: { prefix: '/api' } });
  await server.register({ plugin: require('../../interfaces/routes/genre'), routes: { prefix: '/api' } });
  await server.register({ plugin: require('../../interfaces/routes/marker'), routes: { prefix: '/api' } });
  await server.register({ plugin: require('../../interfaces/routes/song'), routes: { prefix: '/api' } });
  await server.register({ plugin: require('../../interfaces/routes/user'), routes: { prefix: '/api' } });
  await server.register({ plugin: require('../../interfaces/routes/role'), routes: { prefix: '/api' } });

  server.app.serviceLocator = require('../../infrastructure/config/service-locator');

  await CreateDefaultAdministratorUser(server.app.serviceLocator);
  return server;
};

module.exports = createServer;

async function CreateDefaultAdministratorUser(serviceLocator) {
  const role = await addRole('Administrator', serviceLocator);
  await createAdminUser(role, serviceLocator);
}

async function createAdminUser(role, serviceLocator) {
  var user = Object();
  user.firstName = 'Administrator';
  user.lastName = 'Administrator';
  user.email = 'admin@radiusradio.com';
  user.password = 'Default.1234';
  user.roleId = role.id;
  if (!(await getByEmail(user.email, serviceLocator))) {
    await addUser(user, 'Administrator', serviceLocator);
  }
}