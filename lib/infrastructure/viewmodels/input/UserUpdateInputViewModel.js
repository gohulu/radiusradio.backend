'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email({ tlds: { allow: false } }).required(),
    roleId: Joi.number().required(),
}).label('UserUpdateInputViewModel');