'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email({ tlds: { allow: false } }).required(),
    password: Joi.string().required(),
    roleId: Joi.number().required()
}).label('UserAddInputViewModel');