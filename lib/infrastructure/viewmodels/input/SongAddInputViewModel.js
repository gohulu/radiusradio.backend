'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    name: Joi.string().required(),
    artistId: Joi.number().required(),
    genreId: Joi.number().required(),
}).label('SongAddInputViewModel');