'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number().required(),
    description: Joi.string().required(),
    latitude: Joi.string().required(),
    longitude: Joi.string().required(),
    songs: Joi.array().items(Joi.number())
}).label('MarkerSaveInputViewModel');