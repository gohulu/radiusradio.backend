'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number(),
    name: Joi.string().required()
}).label('GenreSaveInputModel');