'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number().required(),
    name: Joi.string().required(),
    artistId: Joi.number().required(),
    genreId: Joi.number().required(),
}).label('SongSaveInputViewModel');