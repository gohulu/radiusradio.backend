'use strict';

module.exports = class {
    constructor(id = null, latitude, longitude, description) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
    }
};