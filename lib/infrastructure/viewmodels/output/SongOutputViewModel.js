'use strict';

module.exports = class {
    constructor(id = null, artistId, artistName, genreId, genreName, name, filename) {
        this.id = id;
        this.artistId = artistId;
        this.artistName = artistName;
        this.genreId = genreId;
        this.genreName = genreName;
        this.name = name;
        this.filename = filename;
    }
};