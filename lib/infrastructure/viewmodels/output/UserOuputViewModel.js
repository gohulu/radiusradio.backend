'use strict';

module.exports = class {
    constructor(id = null, firstName, lastName, email, active, roleId, roleName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.active = active;
        this.role = new Object();
        this.role.id = roleId;
        this.role.name = roleName;
    }
};