'use strict';

module.exports = class {
    constructor(id = null, name) {
        this.id = id;
        this.name = name;
    }
};