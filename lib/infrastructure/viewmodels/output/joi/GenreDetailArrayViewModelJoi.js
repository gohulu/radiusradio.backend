const GenreDetailViewModelJoi = require('./GenreDetailViewModelJoi');

const Joi = require("joi");

module.exports = Joi.array().items(GenreDetailViewModelJoi).label('GenreDetailArrayViewModel');