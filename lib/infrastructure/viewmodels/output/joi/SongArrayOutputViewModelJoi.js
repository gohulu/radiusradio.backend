const SongOutputViewModel = require('./SongOutputViewModelJoi');

const Joi = require("joi");

module.exports = Joi.array().items(SongOutputViewModel).label('SongArrayOutputViewModel');