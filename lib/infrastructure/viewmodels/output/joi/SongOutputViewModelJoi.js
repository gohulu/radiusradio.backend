'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number().required(),
    name: Joi.string().required(),
    artistId: Joi.number().required(),
    artistName: Joi.string().required(),
    genreId: Joi.number().required(),
    genreName: Joi.string().required(),
    filename: Joi.any()
}).label('SongOutputViewModel');