const ArtistDetailViewModelJoi = require('./ArtistDetailViewModelJoi');

const Joi = require("joi");

module.exports = Joi.array().items(ArtistDetailViewModelJoi).label('ArtistDetailArrayViewModel');