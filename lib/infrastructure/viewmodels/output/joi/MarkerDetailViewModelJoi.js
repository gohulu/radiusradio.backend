'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number().required(),
    latitude: Joi.string().required(),
    longitude: Joi.string().required(),
    description: Joi.string().required(),
}).label('MarkerDetailViewModel');