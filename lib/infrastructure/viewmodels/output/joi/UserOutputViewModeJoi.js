'use strict';

const Joi = require("joi");

module.exports = Joi.object({
    id: Joi.number().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    active: Joi.boolean().required(),
    role: Joi.object({
        id: Joi.number().required(),
        name: Joi.string().required()
    })
}).label('UserOutputViewModel');