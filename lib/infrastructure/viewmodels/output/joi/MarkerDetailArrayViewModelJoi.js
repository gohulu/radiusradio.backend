const Joi = require("joi");
const MarkerDetailViewModelJoi = require('./MarkerDetailViewModelJoi');

module.exports = Joi.array().items(MarkerDetailViewModelJoi).label('MarkerDetailArrayViewModel');