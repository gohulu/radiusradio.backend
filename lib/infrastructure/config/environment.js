'use strict';

const constants = require('./constants');

/**
 * This module centralize all the environment variables of the application. Thanks to this module, there MUST NOT be any
 * `process.env` instruction in any other file or module.
 */
module.exports = (() => {

  const environment = {
    database: {
      dialect: process.env.DATABASE_DIALECT || constants.SUPPORTED_DATABASE.POSTGRES,
      url: "postgres://postgres:H1s4ss0k@127.0.0.1:5432/radius_radio",
    }
  };

  if (process.env.NODE_ENV === 'test') {
    environment.database = {
      driver: constants.SUPPORTED_DATABASE.IN_MEMORY
    }
  } else if (process.env.NODE_ENV === 'development') {
    environment.database = {
      dialect: constants.SUPPORTED_DATABASE.POSTGRES,
      url: "postgres://postgres:H1s4ss0k@127.0.0.1:5432/radius_radio",
    }
  } else if (process.env.NODE_ENV === 'production') {
    environment.database = {
      dialect: constants.SUPPORTED_DATABASE.POSTGRES,
      url: "postgres://postgres:postgres@127.0.0.1:5432/radius_radio",
    }
  }

  return environment;
})();
