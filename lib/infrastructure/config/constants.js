'use strict';

module.exports = {
  SUPPORTED_DATABASE: {
    POSTGRES: 'postgres',
  },
  DefaultPage: 1,
  DefaultPageSize: 50
};
