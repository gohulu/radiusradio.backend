'use strict';

const constants = require('./constants');
const environment = require('./environment');
const JwtAccessTokenManager = require('../security/JwtAccessTokenManager');

function buildBeans() {
  const beans = {
    accessTokenManager: new JwtAccessTokenManager(),
  };

  if (environment.database.dialect === constants.SUPPORTED_DATABASE.POSTGRES) {
    const ArtistRepositoryPostgres = require('../repositories/ArtistRepositoryPostgres');
    const GenreRepositoryPostgres = require('../repositories/GenreRepositoryPostgres');
    const MarkerRepositoryPostgres = require('../repositories/MarkerRepositoryPostgres');
    const SongRepositoryPostgres = require('../repositories/SongRepositoryPostgres');
    const UserRepositoryPostgres = require('../repositories/UserRepositoryPostgres');
    const RoleRepositoryPostgres = require('../repositories/RoleRepositoryPostgres');

    beans.artistRepository = new ArtistRepositoryPostgres();
    beans.genreRepository = new GenreRepositoryPostgres();
    beans.markerRepository = new MarkerRepositoryPostgres();
    beans.songRepository = new SongRepositoryPostgres();
    beans.userRepository = new UserRepositoryPostgres();
    beans.roleRepository = new RoleRepositoryPostgres();
  } else {
    throw new Error('Database not supported');
  }

  return beans;
}

module.exports = buildBeans();