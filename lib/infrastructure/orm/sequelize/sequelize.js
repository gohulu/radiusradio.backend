'use strict';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const { Sequelize } = require('sequelize');
const db = {};
const environment = require("../../config/environment");
const sequelize = new Sequelize(environment.database.url);

var dirname = __dirname + '/models';
fs
    .readdirSync(dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        var model = require(path.join(dirname, file))(sequelize, Sequelize)
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

sequelize.sync();

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;