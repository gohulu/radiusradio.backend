/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('role', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			primaryKey: true,
			autoIncrement: true,
			field: 'Id'
		},
		'name': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'Name'
		}
	}, {
		tableName: 'Role',
		timestamps: false,
	});
};
