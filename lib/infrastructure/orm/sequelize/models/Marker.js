/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	var Marker = sequelize.define('marker', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			primaryKey: true,
			autoIncrement: true,
			field: 'Id'
		},
		'latitude': {
			type: DataTypes.DOUBLE,
			allowNull: false,
			comment: "null",
			field: 'Latitude'
		},
		'longitude': {
			type: DataTypes.DOUBLE,
			allowNull: false,
			comment: "null",
			field: 'Longitude'
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('now'),
			comment: "null",
			field: 'CreatedAt'
		},
		'createdBy': {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: sequelize.fn('now'),
			comment: "null",
			field: 'CreatedBy'
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null",
			field: 'UpdatedAt'
		},
		'updatedBy': {
			type: DataTypes.STRING,
			allowNull: true,
			comment: "null",
			field: 'UpdatedBy'
		},
		'description': {
			type: DataTypes.STRING,
			allowNull: true,
			comment: "null",
			field: 'Description'
		}
	}, {
		tableName: 'Marker'
	});

	Marker.associate = models => {
		Marker.belongsToMany(models.song, {
			through: 'MarkerSong',
			otherKey: 'SongId',
			foreignKey: 'MarkerId'
		})
	};
	
	return Marker;
};
