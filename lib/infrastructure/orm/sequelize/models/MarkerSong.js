/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('markerSong', {
		'markerId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			primaryKey: true,
			references: {
				model: 'Marker',
				key: 'Id'
			},
			field: 'MarkerId'
		},
		'songId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			references: {
				model: 'Song',
				key: 'Id'
			},
			field: 'SongId'
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('now'),
			comment: "null",
			field: 'CreatedAt'
		}
	}, {
		tableName: 'MarkerSong',
		timestamps: false
	});
};
