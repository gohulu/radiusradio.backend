/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('artist', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			primaryKey: true,
			autoIncrement: true,
			field: 'Id'
		},
		'name': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'Name'
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('now'),
			comment: "null",
			field: 'CreatedAt'
		},
		'createdBy': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'CreatedBy'
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null",
			field: 'UpdatedAt'
		},
		'updatedBy': {
			type: DataTypes.STRING,
			allowNull: true,
			comment: "null",
			field: 'UpdatedBy'
		}
	}, {
		tableName: 'Artist'
	});
};
