/* jshint indent: 1 */
module.exports = function(sequelize, DataTypes) {
	const Song = sequelize.define('song', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			primaryKey: true,
			autoIncrement: true,
			field: 'Id'
		},
		'artistId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			references: {
				model: 'Artist',
				key: 'Id'
			},
			field: 'ArtistId'
		},
		'genreId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			references: {
				model: 'Genre',
				key: 'Id'
			},
			field: 'GenreId'
		},
		'name': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'Name'
		},
		'songPath': {
			type: DataTypes.STRING,
			comment: "numm",
			field: 'SongPath'
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.fn('now'),
			comment: "null",
			field: 'CreatedAt'
		},
		'createdBy': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'CreatedBy'
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null",
			field: 'UpdatedAt'
		},
		'updatedBy': {
			type: DataTypes.STRING,
			allowNull: true,
			comment: "null",
			field: 'UpdatedBy'
		},
		'songPath': {
			type: DataTypes.STRING,
			allowNull: true,
			comment: "null",
			field: 'SongPath'
		}
	}, {
		tableName: 'Song'
	});

	Song.associate = models => {
		Song.belongsTo(models.artist);
		Song.belongsTo(models.genre);
		Song.belongsToMany(models.marker, {
			through: models.markerSong,
			foreignKey: 'SongId',
			otherKey: 'MarkerId'
		})
	};

	return Song;
};
