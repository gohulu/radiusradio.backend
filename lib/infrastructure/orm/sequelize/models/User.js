/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const User = sequelize.define('user', {
		'id': {
			type: DataTypes.BIGINT,
			allowNull: false,
			comment: "null",
			primaryKey: true,
			autoIncrement: true,
			field: 'Id'
		},
		'firstName': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'FirstName'
		},
		'lastName': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'LastName'
		},
		'email': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'Email'
		},
		'password': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'Password'
		},
		'active': {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			comment: "null",
			field: 'Active'
		},
		'isLocked': {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			comment: "null",
			field: 'IsLocked'
		},
		'roleId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			references: {
				model: 'Role',
				key: 'Id'
			},
			field: 'RoleId'
		},
		'lastAccessDate': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null",
			field: 'LastAccessDate'
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: false,
			comment: "null",
			field: 'CreatedAt'
		},
		'createdBy': {
			type: DataTypes.STRING,
			allowNull: false,
			comment: "null",
			field: 'CreatedBy'
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null",
			field: 'UpdatedAt'
		},
		'updatedBy': {
			type: DataTypes.STRING,
			allowNull: true,
			comment: "null",
			field: 'UpdatedBy'
		}
	}, {
		tableName: 'User'
	});

	User.associate = models => {
		User.belongsTo(models.role);
	};

	
	return User;
};
