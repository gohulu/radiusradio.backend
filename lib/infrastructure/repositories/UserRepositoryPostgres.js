'use strict';

const sequelize = require('../orm/sequelize/sequelize');
const User = require('../../domain/models/User');
const UserRepository = require('../../domain/repositories/UserRepository');
const UserOutputViewModel = require('../../infrastructure/viewmodels/output/UserOuputViewModel');

module.exports = class extends UserRepository {
    constructor() {
        super();
        this.db = sequelize;
    }

    async getById(id) {
        const u = await this.db.user
        .findByPk(id, {
            include: [{ model: this.db.role }]
        });
    
        return new UserOutputViewModel(u.dataValues.id
            , u.dataValues.firstName
            , u.dataValues.lastName
            , u.dataValues.email
            , u.dataValues.active
            , u.dataValues.role.dataValues.id
            , u.dataValues.role.dataValues.name)
    }

    async getAll() {
        return (await this.db.user.findAll()).map(u => {
            return {
                id: u.dataValues.id, firstName: u.dataValues.firstName,
                lastName: u.dataValues.lastName, email: u.dataValues.email,
                active: u.dataValues.active
            }
        });
    }

    async create(item, createdBy) {
        var data = { firstName: item.firstName, lastName: item.lastName
            , email: item.email, password: item.password, createdBy: createdBy
            , roleId: item.roleId, isLocked: false, active: true
        };
        return await this.db.user.create(data);
    }

    async update(item, updatedBy) {
        var data = { firstName: item.firstName, lastName: item.lastName
            , email: item.email, roleId: item.roleId
            , updatedAt: this.db.Sequelize.fn('NOW'), updatedBy: updatedBy
        };

        var obj = await this.db.user.update(data, { where: { id: item.id } });

        if (obj[0] == 1) {
            return await this.getById(item.id);
        }

        return null;
    }

    async save(item, actionBy) {
        if (item.id > 0) {
            return await this.update(item, actionBy);
        }

        return await this.create(item, actionBy);
    }

    async getByEmail(email) {
        var user = await this.db.user.findOne({ where: { email: email }});
        if (user == null) {
            return user;
        }
        return user.dataValues;
    }
}