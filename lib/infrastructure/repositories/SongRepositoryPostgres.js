'use strict';

const sequelize = require('../orm/sequelize/sequelize');
const SongRepository = require('../../domain/repositories/SongRepository');
const SongOutputViewModel = require('../../infrastructure/viewmodels/output/SongOutputViewModel');

module.exports = class extends SongRepository {
    constructor() {
        super();
        this.db = sequelize;
    }

    async getById(id) {
        const obj = await this.db.song.findByPk(id, {
            include:[ 
                { model: this.db.artist,  required: false },
                { model: this.db.genre, required: false }
            ]});

        if (!obj) {
            return 'item not found'
        }
        return BuildSongOutputViewModel(obj);
    }

    async getAll() {
        var result = (await this.db.song.findAll({
            include:[ 
                    { model: this.db.artist,  required:false },
                    { model: this.db.genre, required:false }
                ]})).map(s => { return BuildSongOutputViewModel(s) });
        return result;
    }

    async getAllByMarkerId(markerId) {
        return (await this.db.song.findAll({
                include: [ 
                    { model: this.db.marker, required: false},
                    { model: this.db.artist,  required:false },
                    { model: this.db.genre, required:false },
                ],
                where: { '$markers.Id$' : markerId }
            })).map(s => { 
                return BuildSongOutputViewModel(s) 
            });
    }

    async create(item, createdBy) {
        var data = {
            name: item.name,
            artistId: item.artistId,
            genreId: item.genreId,
            createdBy: createdBy
        };

        var created = await this.db.song.create(data);
        return created;
    }

    async update(item, updatedBy) {
        var current = await this.getById(item.id);

        if (current == null) {
            throw Error('entity does not exists');
        }

        var data = {
            name: item.name,
            artistId: item.artistId,
            genreId: item.genreId,
            updatedAt: this.db.Sequelize.fn('NOW'),
            updatedBy: updatedBy
        };

        var obj = await this.db.song.update(data, {
            where: { id: item.id }
        });


        if (obj[0] == 1) {
            return await this.getById(item.id);
        }

        return null;
    }

    async save(item, actionBy) {
        if (item.id > 0) {
            return await this.update(item, actionBy);
        }

        return await this.create(item, actionBy);
    }

    async addSongFile(id, filePath, actionBy) {
        var data = { songPath : filePath
            , updatedAt: this.db.Sequelize.fn('NOW'), updatedBy: actionBy };

        var obj = await this.db.song.update(data, { where: { id: id } });

        if (obj[0] == 1) {
            return await this.getById(id);
        }

        return null;
    }
}

function BuildSongOutputViewModel(obj) {
    return new SongOutputViewModel(obj.dataValues.id, obj.dataValues.artistId,
        obj.dataValues.artist.dataValues.name, obj.dataValues.genreId,
        obj.dataValues.genre.name, obj.dataValues.name, obj.dataValues.songPath);
}
