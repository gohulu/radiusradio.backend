'use strict';

const sequelize = require('../orm/sequelize/sequelize');
const Artist = require('../../domain/models/Artist');
const ArtistRepository = require('../../domain/repositories/ArtistRepository');
const ArtistDetailViewModel = require('../viewmodels/output/ArtistDetailViewModel');

module.exports = class extends ArtistRepository {
    constructor() {
        super();
        this.db = sequelize;
    }

    async getById(id) {
        const obj = await this.db.artist.findByPk(id);
        return new ArtistDetailViewModel(obj.id, obj.name);
    }

    async getAll() {
        return (await this.db.artist.findAll()).map(artist => { 
            return new ArtistDetailViewModel(artist.dataValues.id, artist.dataValues.name); } );
    }

    async create(item, createdBy) {
        var data = { name: item.name, createdBy: createdBy };
        return await this.db.artist.create(data);
    }

    async update(item, updatedBy) {
        var data = {
            name: item.name,
            updatedAt: this.db.Sequelize.fn('NOW'),
            updatedBy: updatedBy
        };

        var obj = await this.db.artist.update(data, { where: { id: item.id } });

        if (obj[0] == 1) {
            return await this.getById(item.id);
        }

        return null;
    }

    async save(item, actionBy) {
        if (item.id > 0) {
            return await this.update(item, actionBy);
        }

        return await this.create(item, actionBy);
    }
}