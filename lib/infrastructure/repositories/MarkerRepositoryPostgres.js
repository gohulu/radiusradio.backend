'use strict'

const sequelize = require('../orm/sequelize/sequelize');
const MarkerRepository = require('../../domain/repositories/MarkerRepository');
const MarkerDetailViewModel = require('../../infrastructure/viewmodels/output/MarkerDetailViewModel');

module.exports = class extends MarkerRepository {
    constructor() {
        super();
        this.db = sequelize;
    }

    async getById(id) {
        const obj = await this.db.marker.findByPk(id)

        if (!obj) {
            return 'item not found'
        }
        return new MarkerDetailViewModel(obj.dataValues.id
            , obj.dataValues.latitude, obj.dataValues.longitude, obj.dataValues.description)
    }

    async getAll() {
        return (await this.db.marker.findAll())
            .map(m => new MarkerDetailViewModel(m.dataValues.id
                , m.dataValues.latitude, m.dataValues.longitude, m.dataValues.description));
    }

    async create(item) {
        var data = {
            description: item.description,
            latitude: item.latitude,
            longitude: item.longitude,
            createdBy: item.createdBy
        };

        var created = await this.db.marker.create(data);

        var msArray = new Array();

        for (var i = 0; i < item.songs.length; i++) {
            var ms = {
                markerId: created.id,
                songId: item.songs[i],
            }
            msArray.push(ms);
        }

        await this.db.markerSong.bulkCreate(msArray);
        return created;
    }

    async update(item) {
        var current = await this.getById(item.id);

        if (current == null) {
            throw Error('entity does not exists');
        }

        var data = {
            description: item.description,
            latitude: item.latitude,
            longitude: item.longitude,
            updatedAt: this.db.sequelize.fn('NOW'),
            updatedBy: item.updatedBy
        };

        var updated = await this.db.marker.update(data, {
            where: { id: item.id }
        });

        var msArray = new Array();

        for (var i = 0; i < item.songs.length; i++) {
            var ms = {
                markerId: current.id,
                songId: item.songs[i],
            }
            msArray.push(ms);
        }

        await this.db.markerSong.destroy({ where: { markerId: [item.id] } });
        await this.db.markerSong.bulkCreate(msArray);

        if (updated[0] == 1) {
            return await this.getById(item.id);
        }

        return null;
    }

    async save(item) {
        if (item.id > 0) {
            return await this.update(item);
        }

        return await this.create(item, actionBy);
    }

    async setCoordinates(item, actionBy) {
        var current = await this.getById(item.id);

        if (current == null) {
            throw Error('entity does not exists');
        }

        var data = {
            latitude: item.latitude,
            longitude: item.longitude,
            updatedAt: this.db.sequelize.fn('NOW'),
            updatedBy: actionBy
        };

        var obj = await this.db.marker.update(data, {
            where: { id: item.id }
        });

        if (obj[0] == 1) {
            return await this.getById(item.id);
        }

        return null;
    }
}