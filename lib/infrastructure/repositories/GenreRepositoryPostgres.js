'use strict';

const sequelize = require('../orm/sequelize/sequelize');
const Genre = require('../../domain/models/Genre');
const GenreRepository = require('../../domain/repositories/GenreRepository');
const GenreDetailViewModel = require('../../infrastructure/viewmodels/output/GenreDetailViewModel');

module.exports = class extends GenreRepository {
    constructor() {
        super();
        this.db = sequelize;
    }

    async getById(id) {
        const obj = await this.db.genre.findByPk(id);

        if (!obj) {
            return false;
        }
        return new GenreDetailViewModel(obj.id, obj.name);
    }

    async getAll() {
        return (await this.db.genre.findAll())
            .map(g => new GenreDetailViewModel(g.dataValues.id, g.dataValues.name));
    }

    async create(item, createdBy) {
        var data = {
            name: item.name,
            createdBy: createdBy
        };

        var created = await this.db.genre.create(data);
        return created;
    }

    async update(item, updatedBy) {
        var current = await this.getById(item.id);

        if (current == null) {
            throw Error('entity does not exists');
        }

        var data = {
            name: item.name,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedBy
        };

        var obj = await this.db.genre.update(data, {
            where: { id: item.id }
        });


        if (obj[0] == 1) {
            return await this.getById(item.id);
        }

        return null;
    }

    async save(item, actionBy) {
        if (item.id > 0) {
            return await this.update(item, actionBy);
        }

        return await this.create(item, actionBy);
    }
}