'use strict';

const sequelize = require('../orm/sequelize/sequelize');
const RoleRepository = require('../../domain/repositories/RoleRepository');
const RoleDetailViewModel = require('../../infrastructure/viewmodels/output/RoleDetailViewModel');

module.exports = class extends RoleRepository {
    constructor() {
        super();
        this.db = sequelize;
    }

    async getAll() {
        return (await this.db.role.findAll()).map(u => {
            return { id: u.dataValues.id, name: u.dataValues.name }
        });
    }

    async create(name) {
        var created = await this.db.role.create({ name: name });
        return created;
    }

    async getByName(name) {
        var role = await this.db.role.findOne({ where: { name: name }});
        if (!role) {
            return null;
        }
        return new RoleDetailViewModel(role.dataValues.id, role.dataValues.name);
    }
}